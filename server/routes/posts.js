import express from 'express'
import { createProducer, deleteProducer, getAllProducers } from '../controllers/producerController.js'
import {authUser, getUsers, verifyJwt, saveUser, getUser} from '../controllers/userController.js'
import { createProduct, deleteProduct, getAllProducts, getOneProduct } from '../controllers/productController.js'
const router = express.Router()

// PRODUCERS UPITI
router.get('/getProducers', getAllProducers )
router.post('/createProducer', createProducer)
router.get('/deleteProducer', deleteProducer)
//router.get('/posts', createPost )

// PRODUCT UPITI
router.get('/getProducts', getAllProducts)
router.get('/getOneProduct', getOneProduct)
router.post('/createProduct', createProduct)
router.get('/deleteProduct', deleteProduct)
/// REGISTRACIJA I LOGIRANJE
router.post('/registration', saveUser) //working b
router.post('/login', authUser)

router.get('/users', getUsers)
router.get('/user', getUser)
router.post('/verifyIt',verifyJwt)

/// USER UPITI

   
export default router