import mongoose from 'mongoose'


const productSchema = mongoose.Schema({
    title: String,
    price:String,
    producer:String,
    alcPerc:String,
    colour:String,
    type:String,
    details:String,
    url:String,
    idProducer:String

})

const Product = mongoose.model('Product', productSchema)

export default Product