import mongoose from 'mongoose'
//Proizvođač treba imati neke podatke o sebi kao što su godina osnivanja, država, kratki opis firme,
 ///   primjerice url na logo... 

const producerSchema = mongoose.Schema({
    title: String,
    year:String,
    country:String,
    desc:String,
    urlIm:String
})

const Producer = mongoose.model('Producer', producerSchema)

export default Producer