import Producer from '../models/producer.js'
import User from '../models/user.js'
import  Mongoose   from 'mongoose'
import Product from '../models/product.js'

// amo cu za fetchanje producera
export const getAllProducers = async (req, res) => {
    try {
        Producer.find({}, (error, result) =>{
            if(error){
                res.send(error)}
            res.send(result)
        })
    } catch (error) {
        res.status(404).json({message : error.message})
    }
}

export const getProducer = async(req, res) => {
    try {
    const what =  req.query.title
    console.log("OVO SE trazi producer" +what)
    await Producer.find({title: what}).then(
       (error, result) =>{
        if(error){
           return res.send(error)
        }
        console.log(result)
       return res.send(result)
        
    })
    } catch (error) {
        res.status(404).json({message : error.message})
    }
}


export const createProducer =  (req, res) => {
    const email = req.body.email
    console.log(email)
    if(email === undefined)
    {
        console.log("uslo u nije admin")
        return res.status(401).json({message : "Unauthorized for this action"})
    }
    User.find({email: email}).then((result, error) =>{
        console.log(result)
        if(error){
           console.log("Error: "+error)
        }
        try {
        if ( "ADMIN" !== result[0].role) {
            
          return res.status(401).json({message : "Unauthorized for this action"})

          }
          const producer = new Producer({title:req.body.title, year:req.body.year, desc:req.body.desc,
            urlIm:req.body.url, country:req.body.country})
        
        
            producer.save()
        return res.status(200)
        } catch (error) {
            
            res.status(401).json({message : "Unauthorized!"})
    }})
}


export const deleteProducer = async (req, res) =>{
    try {
        const id =  req.query.prod
        console.log(id)
        const monId = new Mongoose.Types.ObjectId(id)
        console.log("id mongose- "+ monId)
        Product.findOne({idProducer: monId}).then((result, error)=>{
            if(error){
                console.log("Error: "+error)
                return res.send(error)
            }
            console.log(result)
            if(result.length !== 0 )
            {
                return res.status(200).json({message: "Producer have its products"})
            }
            Producer.deleteOne({ _id: monId }, function(err) {
            if (!err) {
                    return res.status(400).json({message: "Deleting unsuccesful"})
            }
            else {
                    return res.status(200).json({message: "Success"})
            }
        });
        })
        } catch (error) {
            res.status(404).json({message : error.message})
        }
}