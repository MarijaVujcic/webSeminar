import Producer from '../models/producer.js'
import Product from '../models/product.js'
import User from '../models/user.js'
import  Mongoose   from 'mongoose'

export const getAllProducts = async (req, res) => {
    try {
        Product.find({}, (error, result) =>{
            if(error){
                return res.send(error)}
            return res.send(result)
        })
    } catch (error) {
        res.status(404).json({message : error.message})
    }
}

export const getOneProduct = async (req, res) =>
{
    const id = req.query.prod
    const monId = new Mongoose.Types.ObjectId(id)
    console.log("id mongose- "+ monId)
    try{
    Product.findOne({_id: monId}).then((result, error)=>{
        if(error){
            console.log("Error: "+error)
            return res.send(error)
        }
        console.log(result)
        return res.send(result)
    })}
    catch(error)
    {
        console.log(error)
    }
}

export const createProduct = async (req, res) => {
    const email = req.body.email
    
    User.find({email: email}).then((result, error) =>{
        if(error){
           console.log("Error: "+error)
        }
       try {
       if ("ADMIN" !== result[0].role) {
         return res.status(401).json({message : "Unauthorized for this action"})
           
       }
       else{
           console.log("uslo u else"+email)
        const title = req.body.title
        const price = req.body.price
        const producer = req.body.producer
        const alcPerc = req.body.alcPerc
        const colour = req.body.colour
        const type = req.body.type
        const details = req.body.details
        const url = req.body.url
        const idProducer = req.body.idProducer
        const newProduct = new Product({title:title, price:price, producer:producer, alcPerc: alcPerc
        , colour:colour, type:type, details:details, url:url, idProducer:idProducer})
            try {
                 newProduct.save()
                 return res.status(201).json(newProduct)
            } catch (error) {
                return res.status(404).json({message : error.message})
            }
       }
    }catch(e)
       {

       }}
    )
}

export const deleteProduct = async (req, res) =>{
        try {
            const id =  req.query.prod
            console.log(id)
            const monId = new Mongoose.Types.ObjectId(id)
            console.log("id mongose- "+ monId)
            await Product.deleteOne({ _id: monId }, function(err) {
                if (!err) {
                        return res.status(400).json({message: "Deleting unsuccesful"})
                }
                else {
                        return res.status(200).json({message: "Success"})
                }
            });
            } catch (error) {
                res.status(404).json({message : error.message})
            }
    
}
