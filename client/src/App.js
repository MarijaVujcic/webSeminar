import React, {useEffect, useState} from 'react'
import { Container} from '@material-ui/core';
import { Route} from 'react-router-dom'
import RegistrationForm from './components/Registration/RegistrationForm';
import LoginForm from './components/Login/LoginForm';
import Navigation from './components/Welcome/NavigationForm';
import ListOfProducts from './components/ProductControll/ListOfProducts';
import { Logout } from './components/Login/Logout';
import ListOfProducers from './components/ProducerControll/ListOfProducers';
import AddProductForm from './components/ProductControll/AddProductForm';
import AddProducerForm from './components/ProducerControll/AddProducerForm';
import DetailedProduct from './components/ProductControll/DetailedProduct';
import Box from '@material-ui/core/Box'

/**Web mjesto će se sastojati od stranice sa pregledom
proizvoda, stranice s detaljima, stranice sa svim proizvođačima, stranice za unos novog artikla, stranice
za unos novog proizvođača 
 */
const App = () =>
{
    const [isVer, setIsVer] = useState(false)
    useEffect(()=>
    {
        if(localStorage.getItem("token") != null){
             setIsVer(true)
            console.log("uslo je u mainu")
            }
        else{     
        setIsVer(false)
        console.log("uslo u falsw")}
    })
    return (
    <Container maxwidth="lg" >
        <Navigation isVer={isVer}>
        </Navigation>
        <Box >
        <Route path="/registration" component={RegistrationForm} />
        <Route path="/login" component={LoginForm} />
        <Route path="/logout" component={Logout}/>
        <Route path="/producers" component={ListOfProducers}/>
        <Route path="/products" component={ListOfProducts}/>
        <Route path="/addProduct" component={AddProductForm}/>
        <Route path="/addProducer" component={AddProducerForm}/>
        <Route path="/showDetailsP/:productId" component={DetailedProduct} />
        </Box>
    </Container>)
}

export default App;