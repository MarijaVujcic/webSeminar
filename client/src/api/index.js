import axios from 'axios'
import { useHistory } from 'react-router-dom'

// upiti prema bazi kao
const url = 'http://localhost:5000/'

export const fetchUsers = () => axios.get(url+'users').catch(function (error) {
    window.location.replace("/products")
});

export const fetchUserExist = (usermail) => axios.get(url+'user?email=' + usermail).then((response)=>
{
    console.log( response.data)
    return response.data
})

export const authenticate = (data) => axios.post(url+'login', data)
export const saveUserRegister = (data) => axios.post(url+'registration', data)
export const verifyIt = (data) => axios.post(url+'verifyIt', data).catch(function (error) {
    window.location.replace("/login")
});


export const fetchProducts = () => axios.get(url+'getProducts').catch(function (error) {
    window.location.replace("/")
});

export const fetchOneProduct = (data) => axios.get(url+'getOneProduct?prod='+ data).catch(function (error) {
    window.location.replace("/")
});

export const fetchOneProducer = (data) => axios.get(url+'getOneProducer', data).catch(function (error) {
    window.location.replace("/")
});

export const fetchProducers = () => axios.get(url+'getProducers').catch(function (error) {
    window.location.replace("/")
});

export const saveProduct=(data)=>axios.post(url + 'createProduct',data).catch(function (error) {
    window.location.replace("/")
});

export const saveProducer=(data)=>axios.post(url + 'createProducer', data).catch(function (error) {
    window.location.replace("/")
});

export const deleteProducer=(data) => axios.get(url +'deleteProducer?prod='+data)
export const deleteProduct=(data) => axios.get(url +'deleteProduct?prod='+data)

