import React, {useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { Snackbar } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import { fetchUserExist, saveUserRegister } from '../../api';
import { Redirect } from 'react-router-dom';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '60%', // Fix IE 11 issue.
    height: '60%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function RegistrationForm() {

  const classes = useStyles();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [username, setUsername] = useState("")
  const [passwordDos, setPasswordDos] = useState("")
  const [redirect, setRedirect] = useState(false)
  const [exist, setExist] = useState(false)
  const [message, setMessage] = useState("")
  const [open, setOpen] = useState(false)

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  }
  const renderRedirect = () => {
    if (redirect) {
      return <Redirect to='/login' />
    }
  }


  function check(e)
  {
    e.preventDefault();
    if (password !== passwordDos) {
      setMessage("Passwords don't match!")
        handleClick()
        
    }else{
    fetchUserExist(email).then(function(value) {
        if(value.length === 0)
        {
            saveUserRegister({username:username, email:email, password:password, role:"USER" })
            setRedirect(true)
        }
        else{
          console.log("uslo u ima")
          setMessage("User is already registred! Sign in!")
          setExist(true)
          handleClick()
        }
        }) 
      }
    // provjeriti jel user vec postoji
    // DODATI NEKU PROVJERU ZA ROLU TU, TJ DOLI U REGISTER FORMI DA ADMIN MOZE DODAT ADMINAAL NE MORA SE
    
    
  }
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
      {renderRedirect()}
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Register
        </Typography>
        <form className={classes.form} onSubmit={(e) => {check(e);}}>
          <TextField
            variant="outlined" color="secondary"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            onChange={(event)=>{setEmail(event.target.value)}}
          />
          <TextField
            variant="outlined" color="secondary"
            margin="normal"
            required
            fullWidth
            id="username"
            label="Username"
            name="username"
            autoComplete="username"
            autoFocus
            onChange={(event)=>{setUsername(event.target.value)}}
          />
          <TextField
            variant="outlined" color="secondary"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={(event)=>{setPassword(event.target.value)}}
          />
          <TextField
            variant="outlined" color="secondary"
            margin="normal"
            required
            fullWidth
            name="password2"
            label="Repeat password"
            type="password"
            id="password2"
            autoComplete="current-password"
            onChange={(event)=>{setPasswordDos(event.target.value)}}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            variant="outlined" color="secondary"
          >
            Register
          </Button>
          <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
        message={message}
        action={
          <React.Fragment>
            <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
              <CloseIcon fontSize="small" />
            </IconButton>
          </React.Fragment>
        }
      />
        </form>
        
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}