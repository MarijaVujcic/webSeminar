import React from 'react'
import {  Container, Grid } from '@material-ui/core';
import { fetchProducers, fetchProducts, verifyIt } from '../../api';
import ShowProduct from './ShowProduct';
import { Redirect } from 'react-router-dom';

class ListOfProducts extends React.Component
{
    constructor(props)
    {
        super(props)
        this.state = {productList: [], verified:false}
    }
    setList =() =>
    {
        try{
            fetchProducts().then((res)=>{
                this.setState({productList: res.data})
            })
            }catch(error)
            {
                console.log(error)
            }
    }
    
    componentDidMount()
    {
        this.setState({email:localStorage.getItem("email")})
       if (!this.state.verified && verifyIt({token:localStorage.getItem("token")}))
       {
           this.setState({verified:true})
           this.setList()
       }
       else{
        this.setState({verified:false})
       }
    }
    render() {
    return <div>
        <Container>
        <Grid container style={{paddingTop:'10%'}}>
           {this.state.productList.map(product => (
                <Grid item key={product._id} xs={12} md={6} lg={4}>
                    {console.log(product._id)}
                    <ShowProduct product={product}/>
                
                </Grid>
            ))}
          </Grid>
          </Container>
        </div>
    }
}

export default ListOfProducts;