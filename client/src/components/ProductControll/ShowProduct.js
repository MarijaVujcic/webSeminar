import React, {useState, useEffect} from 'react'
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import {Button} from '@material-ui/core'
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert'
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { deleteProduct } from '../../api';
const useStyles = makeStyles((theme) => ({
    root: {
      maxWidth: 345,
    },
    media: {
      height: 0,
      paddingTop: '56.25%', // 16:9
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
    avatar: {
      backgroundColor: red[500],
    },
  }));
  
const ShowProduct = ({product}) =>
{
    const [expanded, setExpanded] = React.useState(false);
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleExpandClick = () => {
    setExpanded(!expanded);
     };
     const MyOptions = [
      "Delete"
    ];
    
    const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
    };
    
    const open = Boolean(anchorEl);
    
    const handleClose = (option) => {
      setAnchorEl(null);
      if(option === "Delete" && localStorage.getItem("role") === "ADMIN")
      {
        deleteProduct(product._id)
        window.location.replace("/")
      }
    };
    const classes = useStyles();

    return(
    <Card className={classes.root} >
        <CardHeader
        avatar={
          <Avatar aria-label="recipe" className={classes.avatar}>
            Wine
          </Avatar>
        }
        action={
          <div>
          <IconButton aria-label="more"
          onClick={handleClick}
          aria-haspopup="true"
        aria-controls="long-menu">
           <MoreVertIcon /> 
          </IconButton>
          <Menu
          anchorEl={anchorEl} 
          keepMounted onClose={handleClose} 
          open={open}>{MyOptions.map((option) => (
            <MenuItem
              key={option} 
              onClick={(e)=>{handleClose(option)}}>
              {option}
            </MenuItem>
          ))}</Menu>
          </div>}
        title={product.title}
      />
      <CardMedia
        className={classes.media}
        image={product.url}
        title={product.title}
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          Price: {product.price}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          Producer: {product.producer}
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>
          
            Alchocol percentage: {product.alcPerc}
        </Typography>
        <Typography paragraph>
            Colour: {product.colour}
            </Typography>
            <Typography paragraph>
            Type: {product.type}
          </Typography>
          <Typography paragraph>
            {product.details}
          </Typography>

        </CardContent>
        
      </Collapse>
      <Button key={product._id} href={'/showDetailsP/' + product._id} color="secondary">
                        Show details
      </Button>
    </Card>
    )
}

export default ShowProduct;