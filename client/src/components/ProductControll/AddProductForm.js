import React from 'react'
import {Container, Box} from '@material-ui/core'
import { fetchProducers, saveProducer, saveProduct } from '../../api'
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '60%', // Fix IE 11 issue.
      height: '60%',
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
    margin: {
        margin: theme.spacing(1),
        marhinBottom: theme.spacing(1)
      }
  }));

class AddProductForm extends React.Component {
    constructor(props){
        super(props)
        this.state = {email:"", title:"", type:"", details:"", url:"", producer:"",
    producerId:"", alcPerc:"", colour:"", price:"", producerList:[]}
    this.onSubmitSave = this.onSubmitSave.bind(this);
    }
    componentDidMount()
    {
        this.setState({email:localStorage.getItem("email")})
        console.log(localStorage.getItem("email"))
        fetchProducers().then((response)=>
        {
          this.setState({producerList:response.data})
        })
        
    }
    onSubmitSave(e)
    {
      e.preventDefault();
      console.log(this.state.producer)
      this.setState({email:localStorage.getItem("email")})
      saveProduct({email:localStorage.getItem("email"), title:this.state.title, price:this.state.price, alcPerc: this.state.alcPerc
        , colour:this.state.colour,producer:this.state.producer, type:this.state.type, details:this.state.details, url:this.state.url, idProducer:this.state.producerId})
      window.location.replace("/")
    }
    render() {
      const { classes } = this.props;
      return <div>
          <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5" style={{color:"darkred"}}>
          Add new product
        </Typography>
        <form className={classes.form} onSubmit={(e) => {this.onSubmitSave(e);}}>
          <TextField
            variant="outlined" color="secondary"
            margin="normal"
            required
            fullWidth
            id="title"
            label="Title"
            name="title"
            autoFocus
            onChange={(event)=>{this.setState({title:event.target.value})}}
          />
          <TextField
            variant="outlined" color="secondary"
            margin="normal"
            fullWidth
            id="prod"
            label="Details"
            name="details"
            autoFocus
            onChange={(event)=>{this.setState({details:event.target.value})}}
          />
          <TextField
            variant="outlined" color="secondary"
            margin="normal"
            required
            fullWidth
            name="type"
            label="Type"
            type="text"
            id="type"
            onChange={(event)=>{this.setState({type:event.target.value})}}
          />
          <TextField
            variant="outlined" color="secondary"
            margin="normal"
            required
            fullWidth
            name="price"
            label="Price Eur."
            type="number"
            id="price"
            onChange={(event)=>{this.setState({price:event.target.value})}}
          />
          <TextField
            variant="outlined" color="secondary"
            margin="normal"
            required
            fullWidth
            name="alcPerc"
            label="Alchochol perc.%"
            type="number"
            id="alcPerc"
            onChange={(event)=>{this.setState({alcPerc:event.target.value})}}
          />
          <TextField
            variant="outlined" color="secondary"
            margin="normal"
            required
            fullWidth
            name="url"
            label="Color"
            type="text"
            id="url"
            onChange={(event)=>{this.setState({colour:event.target.value})}}
          />
          <TextField
            variant="outlined" color="secondary"
            margin="normal"
            required
            fullWidth
            name="url"
            label="Url of image"
            type="text"
            id="url"
            onChange={(event)=>{this.setState({url:event.target.value})}}
          />
          <FormControl className={classes.margin}>
            <InputLabel id="demo-customized-select-label">Producer</InputLabel>
            <Select
            labelId="demo-customized-select-label"
            id="demo-customized-select"
            onChange={(event)=>{this.setState({producer:event.target.value, producerId: event.target.key})}}
            >
            {this.state.producerList.map(producer => (
                <MenuItem value={producer.title} item key={producer._id} xs={12} md={6} lg={4}>
                    {producer.title}
                </MenuItem>
            ))}
            </Select>
          </FormControl>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            variant="outlined" color="secondary"
          >
            ADD
          </Button>
          <Box mt={8}>
       
         </Box>
        </form>
        
      </div>
    </Container>
      </div>;
    }
  }

export default withStyles(useStyles)(AddProductForm)