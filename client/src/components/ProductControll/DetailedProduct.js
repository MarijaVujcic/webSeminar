
import React, {useState, useEffect} from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';

import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import {Button, Container} from '@material-ui/core'
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import { fetchOneProduct } from '../../api';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
    root: {
      maxWidth: 345,
    },
    media: {
      height: "100%",     // as an example I am modifying width and height
    width: '100%',
      paddingTop: '56.25%', // 16:9
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
    avatar: {
      backgroundColor: red[500],
    },
  }));
class DetailedProduct extends React.Component
{
    constructor(props)
    {
        super(props)
        this.state = {details:"", type:"nfirnfirn", price:"", producer:"", producerId:"", color:"", 
        url:"", alcPerc:"", title:"",  isAdmin:false}
        
        
    }
    componentDidMount()
    {
        console.log(this.props.match.params.productId)
        const id = this.props.match.params.productId
        if(localStorage.getItem("role")==="ADMIN")
        {
          this.setState({isAdmin:true})
        }
        fetchOneProduct(id).then((response)=>
        {
            this.setState({details:response.data.details, type:response.data.type, price:response.data.price, 
                producer:response.data.producer, producerId:response.data.producerId, color:response.data.colour, 
                 url:response.data.url, alcPerc:response.data.alcPerc, title: response.data.title })
            console.log(this.state.details)
        })
    }
    render (){
    return <div>
        <Container>
            <Card>
                <CardMedia
        image={this.state.url}
        title={this.state.title}
        />
            </Card>
            <Typography>
                    {this.state.title}
            </Typography>
            <Typography>
            Details:  {this.state.details}
            </Typography>
            <TextField
            hiddenLabel
            id="filled-hidden-label-small"
            defaultValue={this.state.details}
            variant="filled"
            size="medium"
          
            >{this.state.details}</TextField>
            <Typography>
            Type: {this.state.type}
            </Typography>
            <TextField
            hiddenLabel
            id="filled-hidden-label-small"
            defaultValue={this.state.type}
            variant="filled"
            size="medium"
            />
            <Typography>
            Price: {this.state.price}
            </Typography>
            <TextField
            hiddenLabel
            id="filled-hidden-label-small"
            defaultValue={this.state.price}
            variant="filled"
            size="medium"
            />
            <Typography>
             Alchocol perc: {this.state.alcPerc}
            </Typography>
            <TextField
            hiddenLabel
            id="filled-hidden-label-small"
            defaultValue={this.state.alcPerc}
            variant="filled"
            size="medium"
            />
            <Typography>
            Producer: {this.state.producer}
            </Typography>
            <TextField
            hiddenLabel
            id="filled-hidden-label-small"
            defaultValue={this.state.producer}
            variant="filled"
            size="medium"
            />
            <Button>
              Change
            </Button>
        </Container>
        </div>
    }
}

export default DetailedProduct