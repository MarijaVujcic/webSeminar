import React, {useState, useEffect} from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import IconButton from '@material-ui/core/IconButton';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert'
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { deleteProducer } from '../../api';

const useStyles = makeStyles((theme) => ({
    root: {
      maxWidth: 345,
    },
    media: {
      height: 0,
      paddingTop: '56.25%', // 16:9
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
    avatar: {
      backgroundColor: red[500],
    },
  }));
  
const ShowProducer = ({producer}) =>
{
    const [anchorEl, setAnchorEl] = React.useState(null);
  
    const MyOptions = [
      "Delete"
    ];
    
    const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
      console.log(event.target.value)
    };
    
    const open = Boolean(anchorEl);
    
    const handleClose = (option) => {
      setAnchorEl(null);
      console.log(option)
      // if admin dodat provjeru
      if(option === "Delete")
      {
        deleteProducer(producer._id)
        window.location.replace("/")
      }
    };
    const classes = useStyles();
    return(
    <Card className={classes.root} sx={{ maxWidth: 345 }}>
        <CardHeader
        action={
          <div>
          <IconButton aria-label="more"
          onClick={handleClick}
          aria-haspopup="true"
        aria-controls="long-menu">
           <MoreVertIcon /> 
          </IconButton>
          <Menu
          anchorEl={anchorEl} 
          keepMounted onClose={handleClose} 
          open={open}>{MyOptions.map((option) => (
            <MenuItem
              key={option}
              value={option}
              onClick={(e)=>{handleClose(option)}}>
              {option}
            </MenuItem>
          ))}</Menu>
          </div>

        }
        title={producer.title}
      />
      <CardMedia
        className={classes.media}
        image={producer.urlIm}
        title={producer.title}
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          Year of making - {producer.year}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          Country  {producer.country}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          {producer.desc}
        </Typography>
      </CardContent>
      
   
    </Card>
    )
}

export default ShowProducer;