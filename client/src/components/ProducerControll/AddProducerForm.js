import React, {useHistory} from 'react'
import {Container} from '@material-ui/core'
import { saveProducer } from '../../api'
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { Redirect } from 'react-router-dom'

const useStyles = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '60%', // Fix IE 11 issue.
      height: '60%',
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));
class AddProducerForm extends React.Component {
    constructor(props){
        super(props)
        this.state = {email:"", title:"", year:"", desc:"", urlIm:"", country:"", isSaved:false}
        this.onSubmitSave = this.onSubmitSave.bind(this);
        this.redirectMe = this.redirectMe.bind(this)
    }
    redirectMe()
    {
      return <Redirect to="/showProducers"/>
    }
  
    componentDidMount()
    {
      this.setState({email:localStorage.getItem("email")})
        
    }
    onSubmitSave(e)
    {
      console.log("uslo u on submit" + this.stateemail)
      this.setState({email:localStorage.getItem("email")})
      e.preventDefault();
      saveProducer({email:this.state.email, title:this.state.title, year:this.state.year, desc:this.state.desc,
         url:this.state.urlIm, country:this.state.country})
         window.location.replace("/producers")
    }
    render() {
      const { classes } = this.props;
      return <div>
          <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5" style={{color:"darkRed"}}>
          Add new producer
        </Typography>
        <form className={classes.form} onSubmit={(e) => {this.onSubmitSave(e);}}>
          <TextField
            variant="outlined" color="secondary"
            margin="normal"
            required
            fullWidth
            id="title"
            label="Title"
            name="title"
            autoFocus
            onChange={(event)=>{this.setState({title:event.target.value})}}
          />
          <TextField
            variant="outlined" color="secondary"
            margin="normal"
            fullWidth
            id="prod"
            label="Details"
            name="details"
            multiline
            autoFocus
            onChange={(event)=>{this.setState({desc:event.target.value})}}
          />
          <TextField
            variant="outlined" color="secondary"
            margin="normal"
            required
            fullWidth
            name="year"
            label="Year"
            type="text"
            id="alcPerc"
            onChange={(event)=>{this.setState({year:event.target.value})}}
          />
          <TextField
            variant="outlined" color="secondary"
            margin="normal"
            required
            fullWidth
            name="type"
            label="Country"
            type="text"
            id="type"
            onChange={(event)=>{this.setState({country:event.target.value})}}
          />
          <TextField
            variant="outlined" color="secondary"
            margin="normal"
            required
            fullWidth
            name="type"
            label="Url of image"
            type="text"
            id="type"
            onChange={(event)=>{this.setState({urlIm:event.target.value})}}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            variant="outlined" color="secondary"
          >
            ADD
          </Button>
        </form>
        
      </div>
    </Container>
      </div>;
    }
  }

export default withStyles(useStyles)(AddProducerForm)