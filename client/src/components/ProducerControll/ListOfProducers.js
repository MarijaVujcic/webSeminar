import React from 'react'
import { Container, Grid } from '@material-ui/core';
import { fetchProducers, verifyIt } from '../../api';
import ShowProducer from './ShowProducers';
import { Redirect } from 'react-router-dom';

class ListOfProducers extends React.Component
{
    constructor(props)
    {
        super(props)
        this.state = {producerList: [], verified:false}
    }
    setList =() =>
    {
        try{
            fetchProducers().then((res)=>{
                this.setState({producerList: res.data})
            })
            }catch(error)
            {
                console.log(error)
            }
    }
    
    componentDidMount()
    {
        this.setState({email:localStorage.getItem("email")})
        if (!this.state.verified && verifyIt({token:localStorage.getItem("token")}))
        {
            this.setState({verified:true})
            this.setList()
        }
        else{
         this.setState({verified:false})
        }
    }
    render() {
    return <div>
        <Container>
        <Grid container style={{paddingTop:'10%', paddingBottom:'10%'}}>
           {this.state.producerList.map(product => (
                <Grid item key={product._id} xs={12} md={6} lg={4}>
                    <ShowProducer producer={product}/>
                </Grid>
            ))}
          </Grid>
          </Container>

        </div>
    }
}

export default ListOfProducers;