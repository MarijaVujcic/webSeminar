import React, {useState, useEffect} from 'react'
import { Container, Card, Typography, Grow, Grid } from '@material-ui/core';
import {Link, Route} from 'react-router-dom'
import styled from "styled-components";
import { fetchUserExist } from '../../api';
const Navigation = () =>
{
    const linkStyle = {
        margin: "1rem",
        textDecoration: "none",
        color: 'darkRed'
      };

      const [isVer, setIsVer] = useState(true)
      const [isAdmin, setIsAdmin] = useState(false)
      useEffect(()=>
      {
          if(localStorage.getItem("token") !== null){
               setIsVer(false)
               fetchUserExist(localStorage.getItem("email")).then(function(value) {
                if(value[0].role === "ADMIN")
                {
                    setIsAdmin(true)
                    localStorage.setItem("role", "ADMIN")
                }
                else{
                    setIsAdmin(false)
                }
               })
              }
          else{     
          setIsVer(true)
          setIsAdmin(false)
          }
      }, [])
    return(
            <Card maxWidth="lg" align="center">
                    <Typography variant="h3" align="center" style={linkStyle} > Craft wines </Typography>
                    <Link to="/" style={linkStyle}>Home</Link>
               {isVer  && <Link to="/registration" style={linkStyle}>Registration</Link>}
               {isVer  &&  <Link to="/login" style={linkStyle}>Login</Link>}
               {isAdmin  && <Link to="/addProduct" style={linkStyle}>Add Product</Link> }
               {isAdmin  && <Link to="/addProducer" style={linkStyle}>Add Producer</Link> }
               {!isVer && <Link to="/producers" style={linkStyle}>Show producers</Link>}
               {!isVer && <Link to="/products" style={linkStyle}>Show products</Link>}
                 {!isVer  && <Link to="/logout" style={linkStyle}>Logout</Link>}
            </Card>
    )
}

export default Navigation;