import React, { useState } from 'react';
import { Snackbar } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { authenticate, fetchUserExist } from '../../api';
import { Redirect } from 'react-router-dom';


function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  container:
  {
    backgroundColor: 'black'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '60%',
    marginTop: theme.spacing(1),
    
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function LoginForm() {
  const classes = useStyles();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [registerPlease, setRegisterPlease] = useState(false)
  const [wrongPasswd, setWrongPassword] = useState(false)
  const [redirect, setRedirect] = useState(false)
  const [message, setMessage] = useState("")
  const [open, setOpen] = useState(false)

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  }
  const renderRedirect = () => {
    // namistit da ide na home
    if (redirect) {
      return <Redirect to='/' />
    }
  }
  function saveToken()
  {
    authenticate({email:email, password:password}).then((response)=>{
      console.log("DATA" +response)
      if (response.data.accessToken) {
          localStorage.setItem("token", response.data.accessToken);
          localStorage.setItem("email", email)
          setRedirect(true)
      } else {
          console.log("Authentication error");
      }
  })
  }
  
  function checkIfOk(e)
  {
    e.preventDefault();
    fetchUserExist(email).then(function(value) {
      if(value.length === 0)
      {
        setRegisterPlease(true)
        setMessage("User is not registered! Register first.")
        handleClick()
        return
      }
      else{
        console.log(value[0].password)
        if(value[0].password !== password)
        {
          setMessage("Wrong password!")
          handleClick()
          return
        }
        else
        {
          saveToken()
        }
      }
      })
  }
  return (
    <Container component="main" maxWidth="xs" >
      <CssBaseline />
      <div className={classes.paper}>
      {renderRedirect()}
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Log in
        </Typography>
        <form className={classes.form} onSubmit={(e) => {checkIfOk(e);}} >
          <TextField
            variant="outlined" color="secondary"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            onChange={(e) =>{
              setEmail(e.target.value);
             }}
            onBlur={(e) =>{
              setEmail(e.target.value);
             }}
          />
          <TextField
            variant="outlined" color="secondary"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={(e) =>{
              setPassword(e.target.value);
             }}
            onBlur={(e) =>{
              setPassword(e.target.value);
             }}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            variant="outlined" color="secondary"
            className={classes.submit}
          >
            Log In
          </Button>
          <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={open}
          autoHideDuration={6000}
          onClose={handleClose}
          message={message}
          action={
            <React.Fragment>
              <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                <CloseIcon fontSize="small" />
              </IconButton>
            </React.Fragment>
        }
      />
          <Grid container>
            <Grid item>
              <Link href="/registration" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}