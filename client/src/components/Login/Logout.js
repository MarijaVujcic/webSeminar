import React from "react";
import { Redirect } from 'react-router-dom';

export const Logout = () =>{
    localStorage.removeItem("token")
    localStorage.removeItem("email")
    localStorage.removeItem("role")
    return (

        window.location.replace("/")
    );
};
